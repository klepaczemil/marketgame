# README #


### What is this repository for? ###

* This is code for Engineering Project. Pwr 2016/2017.
* V1.0

### How do I get set up? ###

* Project can be build with gradle and open with AVD (Android Studio).
* Min. API = 22.
* Dependencies are included in repo.
* Database configuration is not needed. Every operations are based on php scrips that app use of.
* Internet permission: On

### Who do I talk to? ###

* Emil Klepacz Politechnika Wrocławska AiR 
* conntact: klepaczemil@gmail.com