package com.klepaczemil.marketgame;

import android.os.Parcel;
import android.os.Parcelable;

public class Wallet implements Parcelable {
    private int wlletId;
    private float money;
    private int MSFT;
    private int GOOGL;
    private int AAPL;
    private int FB;
    private int BHLB;
    private int AMZN;
    private int GNE;
    private int JNJ;

    public int getWlletId() {
        return wlletId;
    }

    public float getMoney() {
        return money;
    }

    public int getMSFT() {
        return MSFT;
    }

    public int getGOOGL() {
        return GOOGL;
    }

    public int getAAPL() {
        return AAPL;
    }

    public int getFB() {
        return FB;
    }

    public int getBHLB() {
        return BHLB;
    }

    public int getAMZN() {
        return AMZN;
    }

    public int getGNE() {
        return GNE;
    }

    public int getJNJ() {
        return JNJ;
    }

    public void setWlletId(int wlletId) {
        this.wlletId = wlletId;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public void setMSFT(int MSFT) {
        this.MSFT = MSFT;
    }

    public void setGOOGL(int GOOGL) {
        this.GOOGL = GOOGL;
    }

    public void setAAPL(int AAPL) {
        this.AAPL = AAPL;
    }

    public void setFB(int FB) {
        this.FB = FB;
    }

    public void setBHLB(int BHLB) {
        this.BHLB = BHLB;
    }

    public void setAMZN(int AMZN) {
        this.AMZN = AMZN;
    }

    public void setGNE(int GNE) {
        this.GNE = GNE;
    }

    public void setJNJ(int JNJ) {
        this.JNJ = JNJ;
    }

    @Override
    public String toString() {
        return "Wallet{" +
                "wlletId=" + wlletId +
                ", money=" + money +
                ", MSFT=" + MSFT +
                ", GOOGL=" + GOOGL +
                ", AAPL=" + AAPL +
                ", FB=" + FB +
                ", BHLB=" + BHLB +
                ", AMZN=" + AMZN +
                ", GNE=" + GNE +
                ", JNJ=" + JNJ +
                '}';
    }

    public Wallet(int wlletId, float money, int MSFT, int GOOGL, int AAPL, int FB, int BHLB, int AMZN, int GNE, int JNJ) {
        this.wlletId = wlletId;
        this.money = money;
        this.MSFT = MSFT;
        this.GOOGL = GOOGL;
        this.AAPL = AAPL;
        this.FB = FB;
        this.BHLB = BHLB;
        this.AMZN = AMZN;
        this.GNE = GNE;
        this.JNJ = JNJ;
    }

    public Wallet() {

    }

    protected Wallet(Parcel in) {
        wlletId = in.readInt();
        money = in.readFloat();
        MSFT = in.readInt();
        GOOGL = in.readInt();
        AAPL = in.readInt();
        FB = in.readInt();
        BHLB = in.readInt();
        AMZN = in.readInt();
        GNE = in.readInt();
        JNJ = in.readInt();
    }

    public int getNumberOfSharesById(String Id) {
        int number = 0;
        switch (Id) {
            case "MSFT": {
                number = this.getMSFT();
                break;
            }
            case "GOOGL": {
                number = this.getGOOGL();
                break;
            }
            case "AAPL": {
                number = this.getAAPL();
                break;
            }
            case "FB": {
                number = this.getFB();
                break;
            }
            case "BHLB": {
                number = this.getBHLB();
                break;
            }
            case "AMZN": {
                number = this.getAMZN();
                break;
            }
            case "GNE": {
                number = this.getGNE();
                break;
            }
            case "JNJ": {
                number = this.getJNJ();
                break;
            }
        }
        return number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(wlletId);
        dest.writeFloat(money);
        dest.writeInt(MSFT);
        dest.writeInt(GOOGL);
        dest.writeInt(AAPL);
        dest.writeInt(FB);
        dest.writeInt(BHLB);
        dest.writeInt(AMZN);
        dest.writeInt(GNE);
        dest.writeInt(JNJ);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Wallet> CREATOR = new Parcelable.Creator<Wallet>() {
        @Override
        public Wallet createFromParcel(Parcel in) {
            return new Wallet(in);
        }

        @Override
        public Wallet[] newArray(int size) {
            return new Wallet[size];
        }
    };
}
