package com.klepaczemil.marketgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signinIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(signinIntent);
            }

        });


        Button companyButton = (Button) findViewById(R.id.company_button);
        companyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent companyListIntent = new Intent(getApplicationContext(), CompanyListActivity.class);
                startActivity(companyListIntent);
            }

        });

    }
}
