package com.klepaczemil.marketgame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class RankingDownload extends AsyncTask<Void, Void, Void> {

    private Context context;
    private ListView rankingListView;
    private ArrayList<RankingPosition> rankingList;
    private ProgressDialog progressDialog;
    private Activity activity;

    private ArrayList<RankingPosition> getRankingList() throws IOException {
        String linkGetRanking = "https://marketgamecom.000webhostapp.com/getTopUsersScript.php?";
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodGetWalletId = new HttpGet(linkGetRanking);
        HttpResponse response = httpClient.execute(getMethodGetWalletId);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");


        ArrayList<RankingPosition> rankingList = new ArrayList<>();


        for (String line; (line = in.readLine()) != null;) {
            RankingPosition rankingPosition = new RankingPosition();
            rankingPosition.setEmail(line.toString());
            line = in.readLine();
            rankingPosition.setMoney(Float.parseFloat(line.toString()));
            System.out.println(rankingPosition.toString());
            rankingList.add(rankingPosition);

        }
        in.close();

        System.out.println(rankingList.toString());
        return rankingList;
    }

    public RankingDownload(Context context, ListView rankingListView, Activity activity){
        this.context = context;
        this.rankingListView = rankingListView;
        this.activity = activity;
    }

    protected void onPreExecute() {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Updating ranking...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(true);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RankingDownload.this.cancel(true);
                progressDialog.dismiss();

            }
        });
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            rankingList = getRankingList();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        progressDialog.dismiss();
        rankingListView.setAdapter(new RankingAdapter(context, rankingList));
    }
}
