package com.klepaczemil.marketgame;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by emil_k on 2016-11-13.
 */

public class MoneyValidator {

    /*if enaugh money return true if not return false*/
    public boolean checkIfEnoughMoney(int numberOfSharesToBuy, float sharePrice, float money) {
        if (((float) numberOfSharesToBuy * sharePrice) <= money) {
            return true;
        }
        return false;
    }

    private float getTransacionPrice(int numberOfSharesToBuy, float sharePrice) {
        float transacionPrice = (float) numberOfSharesToBuy * sharePrice;
        return transacionPrice;
    }

    public boolean checkIfEnoughSheresToSell(String comapnyId, int sharesToSell){
        if(sharesToSell <= StartGameActivity.wallet.getNumberOfSharesById(comapnyId)){
            return true;
        }else{
            return false;
        }
    }

    /*1 - Buy, 0 - Sell*/
    public void changeWalletAfterTransation(int numberOfSharesToBuyOrSell, float sharePrice, String comapnyId, int buyOrSell) {
        /*Change money*/
        float transacionPrice = getTransacionPrice(numberOfSharesToBuyOrSell, sharePrice);
        float moneyBeforeTransaction = StartGameActivity.wallet.getMoney();

        if (buyOrSell == 1) {
            float moneyAfterTransaction = moneyBeforeTransaction - transacionPrice;
            StartGameActivity.wallet.setMoney(moneyAfterTransaction);
        } else {
            float moneyAfterTransaction = moneyBeforeTransaction + transacionPrice;
            StartGameActivity.wallet.setMoney(moneyAfterTransaction);
        }



        /*change number of shares of given companyId*/
        switch (comapnyId) {
            case "MSFT": {
                /*current number of shares + new shares*/
                if (buyOrSell == 1) {
                    int newNumberOfShares = (StartGameActivity.wallet.getMSFT() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setMSFT(newNumberOfShares);
                } else {
                    int newNumberOfShares = (StartGameActivity.wallet.getMSFT() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setMSFT(newNumberOfShares);
                }

                break;
            }
            case "GOOGL": {
                if (buyOrSell == 1) {
                    int newNumberOfShares = (StartGameActivity.wallet.getGOOGL() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setGOOGL(newNumberOfShares);
                } else {
                    int newNumberOfShares = (StartGameActivity.wallet.getGOOGL() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setGOOGL(newNumberOfShares);
                }

                break;
            }
            case "AAPL": {
                if (buyOrSell == 1) {
                    int newNumberOfShares = (StartGameActivity.wallet.getAAPL() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setAAPL(newNumberOfShares);
                } else {
                    int newNumberOfShares = (StartGameActivity.wallet.getAAPL() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setAAPL(newNumberOfShares);
                }

                break;
            }
            case "FB": {
                if (buyOrSell == 1) {
                    int newNumberOfShares = (StartGameActivity.wallet.getFB() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setFB(newNumberOfShares);
                } else {
                    int newNumberOfShares = (StartGameActivity.wallet.getFB() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setFB(newNumberOfShares);
                }

                break;
            }
            case "BHLB": {
                if (buyOrSell == 1) {
                    int newNumberOfShares = (StartGameActivity.wallet.getBHLB() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setBHLB(newNumberOfShares);
                } else {
                    int newNumberOfShares = (StartGameActivity.wallet.getBHLB() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setBHLB(newNumberOfShares);
                }

                break;
            }
            case "AMZN": {
                if(buyOrSell == 1){
                    int newNumberOfShares = (StartGameActivity.wallet.getAMZN() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setAMZN(newNumberOfShares);
                }else{
                    int newNumberOfShares = (StartGameActivity.wallet.getAMZN() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setAMZN(newNumberOfShares);
                }

                break;
            }
            case "GNE": {
                if(buyOrSell == 1){
                    int newNumberOfShares = (StartGameActivity.wallet.getGNE() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setGNE(newNumberOfShares);
                } else{
                    int newNumberOfShares = (StartGameActivity.wallet.getGNE() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setGNE(newNumberOfShares);
                }

                break;
            }
            case "JNJ": {
                if(buyOrSell == 1){
                    int newNumberOfShares = (StartGameActivity.wallet.getJNJ() + numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setJNJ(newNumberOfShares);
                }else{
                    int newNumberOfShares = (StartGameActivity.wallet.getJNJ() - numberOfSharesToBuyOrSell);
                    StartGameActivity.wallet.setJNJ(newNumberOfShares);
                }

                break;
            }
        }

        System.out.println(StartGameActivity.wallet.toString());
    }


    public MoneyValidator() {
    }
}
