package com.klepaczemil.marketgame;


import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DatesPeriod {

    private String startDate = null;
    private String endDate = null;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    LocalDate currentDate;
    private Calendar calendar = new GregorianCalendar();
    private int minusDays;



    /*to create object with given period of time*/
    public DatesPeriod(int minusDays) {
        this.minusDays = minusDays;
        currentDate = new LocalDate();
        setEndDate();
        setStartDate();
    }

    /*to create object witch MAX period of time that allows API*/
    public DatesPeriod() {
        currentDate = new LocalDate();
        setEndDate();
        startDate = "max";
    }


    private void setEndDate() {
        calendar.set(Calendar.YEAR, currentDate.getYear());
        calendar.set(Calendar.MONTH, currentDate.getMonthOfYear() -1);
        calendar.set(Calendar.DAY_OF_MONTH, currentDate.getDayOfMonth());
        System.out.println(sdf.format(calendar.getTime()));
        endDate = sdf.format(calendar.getTime());
    }

    private void setStartDate() {
        calendar.add(Calendar.DAY_OF_MONTH, minusDays);
        startDate = sdf.format(calendar.getTime());
    }

    public String getStartDate() {
        return startDate;
    }



    public String getEndDate() {
        return endDate;
    }


}
