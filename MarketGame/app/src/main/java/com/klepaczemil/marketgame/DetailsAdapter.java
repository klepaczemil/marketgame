package com.klepaczemil.marketgame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import static android.R.attr.data;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class DetailsAdapter extends BaseAdapter {
    Context context;
    ArrayList<Details> details;
    LayoutInflater inflater;

    public DetailsAdapter(Context context, ArrayList<Details> details) {
        this.context = context;
        this.details = details;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return details.size();
    }

    public Object getItem(int position) {
        return details.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = inflater.inflate(R.layout.details_row, null);

        TextView textDetails = (TextView) vi.findViewById(R.id.detailsTextView);
        TextView textDetailsSmall = (TextView) vi.findViewById(R.id.detailsTextViewsmall);

        Details currentDetails = details.get(position);
        textDetails.setText(currentDetails.getCode());
        textDetailsSmall.setText(String.valueOf(currentDetails.getAmount()));
        return vi;

    }


}
