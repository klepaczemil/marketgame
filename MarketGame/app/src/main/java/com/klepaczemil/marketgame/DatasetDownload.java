package com.klepaczemil.marketgame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import android.os.AsyncTask;


import android.widget.TextView;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.Collections;
import java.util.List;

//import static com.klepaczemil.marketgame.R.id.textView;


/*This object download dataset from json and parse them to object
* then pass object to next activitypublic*/

public class DatasetDownload extends AsyncTask<String, Void, String> {
    private ProgressDialog progressDialog;
    private Activity activity;
    private Context context;
    private Dataset dataset;
    private LineChart chart;
    private boolean isFilled;
    private TextView previousCloseTextview;

    /*function return reference to Dataset object created from JSON in http response*/
    private Dataset getDatasetFromHttpRequest(final String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        JSONObject jsonObjectDataset = jsonObject.getJSONObject("dataset");
        String dataset_code = jsonObjectDataset.getString("dataset_code");
        String name = jsonObjectDataset.getString("name");
        String description = jsonObjectDataset.getString("description");
        String start_date = jsonObjectDataset.getString("start_date");
        String end_date = jsonObjectDataset.getString("end_date");

        JSONArray jsonArrayData = jsonObjectDataset.getJSONArray("data");
        ArrayList<Data> dataList = new ArrayList<>();
        for (int i = 0; i < jsonArrayData.length(); i++) {
            String date = jsonArrayData.getJSONArray(i).getString(0);
            double open;
            double high;
            double close;
            double low;
            if (jsonArrayData.getJSONArray(i).getString(1) == "null") {
                open = 0;
            } else {
                open = jsonArrayData.getJSONArray(i).getDouble(1);
            }

            if (jsonArrayData.getJSONArray(i).getString(2) == "null") {
                high = 0;
            } else {
                high = jsonArrayData.getJSONArray(i).getDouble(2);
            }

            if (jsonArrayData.getJSONArray(i).getString(3) == "null") {
                low = 0;
            } else {
                low = jsonArrayData.getJSONArray(i).getDouble(3);
            }

            if (jsonArrayData.getJSONArray(i).getString(4) == "null") {
                close = 0;
            } else {
                close = jsonArrayData.getJSONArray(i).getDouble(4);
            }
            Data data = new Data(date, open, high, low, close);
            dataList.add(data);
        }

        Dataset dataset = new Dataset(dataset_code, name, description, start_date, end_date, dataList);
        return dataset;
    }

    private void printChart(Dataset dataset, boolean isDrawField) {
        /*printing line chart based on Data from dataset object*/
        ArrayList<Data> dataListFromDataset = new ArrayList<>();
        dataListFromDataset = dataset.getDataList();
        /*reverse order for*/
        Collections.reverse(dataListFromDataset);

        List<Entry> entries = new ArrayList<Entry>();
        int index = 0;
        for (Data data : dataListFromDataset) {
            entries.add(new Entry((float) data.getClose(), index));
            index++;
        }
        LineDataSet dataSet = new LineDataSet(entries, "Data Chart: " + dataset.getDataset_code());

        ArrayList<String> labels = new ArrayList<String>();
        for (Data data : dataListFromDataset) {
            labels.add(data.getDate());
        }

        int labelsToSkip = getValueOfLabelsToSkip(dataListFromDataset);

        dataSet.setLineWidth(2.75f);
        dataSet.setCircleRadius(0f);
        dataSet.setDrawFilled(isDrawField);

        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        chart.getXAxis().setLabelsToSkip(labelsToSkip);
        chart.setDescription(dataset.getName());

        LineData lineData = new LineData(labels, dataSet);
        chart.setData(lineData);
        chart.invalidate(); // refresh
    }

    private int getValueOfLabelsToSkip(ArrayList<Data> dataList) {
        int size = dataList.size();
        int labelsToSkip;
        if (size <= 6) {
            labelsToSkip = 0;
        } else if ((6 < size) && (size < 100)) {
            labelsToSkip = 20;
        } else if ((100 <= size) && (size < 190)) {
            labelsToSkip = 50;
        } else if ((190 <= size) && (size < 366)) {
            labelsToSkip = 100;
        } else {
            labelsToSkip = 500;
        }
        return labelsToSkip;
    }

    public DatasetDownload(Activity activity, Context context, LineChart chart, boolean isFilled, TextView previousCloseTextview) {
        this.activity = activity;
        this.context = context;
        this.chart = chart;
        this.isFilled = isFilled;
        this.previousCloseTextview = previousCloseTextview;
    }


    protected void onPreExecute() {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Data processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(true);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatasetDownload.this.cancel(true);
                progressDialog.dismiss();

            }
        });
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        String startDate = arg0[0];
        String endDate = arg0[1];
        String apiCallLink = arg0[2];

        HttpHandler httpHandler = new HttpHandler();
        final String response = httpHandler.makeServiceCall(apiCallLink + startDate);
        try {
            dataset = getDatasetFromHttpRequest(response);
            System.out.println(dataset.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String previousClose = Double.toString(dataset.getDataList().get(0).getClose());
        return (previousClose);


    }

    @Override
    protected void onPostExecute(String result) {

        progressDialog.dismiss();

        System.out.println(result);

        printChart(dataset, isFilled);
        previousCloseTextview.setText(result);

    }


}
