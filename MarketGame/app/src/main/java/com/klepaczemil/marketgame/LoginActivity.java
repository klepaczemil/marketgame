package com.klepaczemil.marketgame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;


public class LoginActivity extends Activity {
    private AutoCompleteTextView emailField;
    private EditText passwordField;
    private TextView statusField;
    private Button signInOrReginsterButton;

    private boolean isEmailCorrect(String email) {
        boolean result = true;
        try {
            InternetAddress emailAdress = new InternetAddress(email);
            emailAdress.validate();
        } catch (AddressException e) {
            result = false;
        }
        return result;
    }

    private boolean isPasswordCorrect(String Password) {
        /**
         * Password should contains
         * one digit
         * a lower case letter
         * an upper case letter
         * whitespace is forbbiden
         * at least 8 characters
         */
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}";
        if (Password.matches(pattern)) {
            return true;
        }
        return false;
    }

    private void showValidationAlert(String alertMessage) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Data validation failure");
        builder.setMessage(alertMessage)
                .setCancelable(false)
                .setIcon(R.drawable.ic_validationkey)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailField = (AutoCompleteTextView) findViewById(R.id.email);
        passwordField = (EditText) findViewById(R.id.password);
        statusField = (TextView) findViewById(R.id.status);
        signInOrReginsterButton = (Button) findViewById(R.id.sign_in_or_register_button);

        signInOrReginsterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String emailTxt = emailField.getText().toString();
                String passwordTxt = passwordField.getText().toString();

                if(!isEmailCorrect(emailTxt)||!isPasswordCorrect(passwordTxt)) {
                /*if e-mail is not corret show validation alert */
                    if (!isEmailCorrect(emailTxt)) {
                        showValidationAlert(getString(R.string.email_validation_alert));
                    }
                /*if password is not formed correctly show validation alert*/
                    if (!isPasswordCorrect(passwordTxt)) {
                        showValidationAlert(getString(R.string.password_validation_alert));
                    }
                }else {
                /*Async Task */
                /*Login Activity.this points to the instance of the Activity you are currently in*/
                    new SigninActivity(LoginActivity.this, getApplicationContext(), statusField).execute(emailTxt, passwordTxt);
                }
            }
        });


    }


}
