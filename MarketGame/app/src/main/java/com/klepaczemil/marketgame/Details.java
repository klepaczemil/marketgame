package com.klepaczemil.marketgame;

/**
 * Created by emil_k on 2016-11-12.
 */

public class Details {
    private String Code; //ex MSFT, GOOGL
    private int amount; // amount of actions
    private String apiLink;

    public Details(String code, int amount, String apiLink) {
        Code = code;
        this.amount = amount;
        this.apiLink = apiLink;
    }

    public String getCode() {
        return Code;
    }

    public int getAmount() {
        return amount;
    }

    public void setCode(String code) {
        Code = code;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getApiLink() {
        return apiLink;
    }

    public void setApiLink(String apiLink) {
        this.apiLink = apiLink;
    }
}
