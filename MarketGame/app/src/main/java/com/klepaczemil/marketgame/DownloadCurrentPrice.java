package com.klepaczemil.marketgame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DownloadCurrentPrice extends AsyncTask<String, Void, String> {
    private ProgressDialog progressDialog;
    private Activity activity;
    private TextView currentPriceTextView;

    public DownloadCurrentPrice(Activity activity, TextView currentPriceTextView){
        this.activity = activity;
        this.currentPriceTextView = currentPriceTextView;
    }

    private double getCurrentPriceFromHttpRequest(final String response) throws JSONException {
        JSONObject jsonObject = new JSONObject(response);
        JSONObject jsonObjectDataset = jsonObject.getJSONObject("dataset");

        JSONArray jsonArrayData = jsonObjectDataset.getJSONArray("data");
        double close = 0.00;
        close = jsonArrayData.getJSONArray(0).getDouble(4);

        return close;
    }

    protected void onPreExecute() {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Updating current price...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(true);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DownloadCurrentPrice.this.cancel(true);
                progressDialog.dismiss();

            }
        });
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        String apiCallLink = arg0[0];

        DatesPeriod dp = new DatesPeriod(-5);
        String startDate = dp.getStartDate();
        double close = 0;

        HttpHandler httpHandler = new HttpHandler();
        final String response = httpHandler.makeServiceCall(apiCallLink + startDate);
        try {
            close  = getCurrentPriceFromHttpRequest(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String previousClose = Double.toString(close);
        return (previousClose);


    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();
        SellActivity.currentSharePrice = Float.parseFloat(result);
        currentPriceTextView.setText(result +" USD.");

    }

}
