package com.klepaczemil.marketgame;

/*helper class made to put instance of it to arrey and next
* use this array to ComapnyListActivity */

public class Company {
    String id;
    String name;
    String description;
    String logoNameDrawable;
    String apiCallLink;

    public String getApiCallLink() {
        return apiCallLink;
    }

    public String getId() {
        return id;
    }


    public Company(String id, String name, String description, String logoNameDrawable) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.logoNameDrawable = logoNameDrawable;

        switch (id) {
            case "MSFT": {
                this.apiCallLink = "https://www.quandl.com/api/v3/datasets/WIKI/MSFT.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
            case "GOOGL": {
                this.apiCallLink = "https://www.quandl.com/api/v3/datasets/GOOG/NASDAQ_GOOGL.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
            case "AAPL": {
                this.apiCallLink = "https://www.quandl.com/api/v3/datasets/GOOG/NASDAQ_AAPL.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
            case "FB": {
                this.apiCallLink = "https://www.quandl.com/api/v3/datasets/GOOG/NASDAQ_FB.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
            case "BHLB": {
                this.apiCallLink = "https://www.quandl.com/api/v3/datasets/GOOG/NASDAQ_BHLB.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
            case "AMZN": {
                this.apiCallLink = "https://www.quandl.com/api/v3/datasets/GOOG/NASDAQ_AMZN.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
            case "GNE": {
                this.apiCallLink ="https://www.quandl.com/api/v3/datasets/GOOG/NYSE_GE.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
            case "JNJ": {
                this.apiCallLink="https://www.quandl.com/api/v3/datasets/GOOG/NYSE_JNJ.json?api_key=rwmpmWRnpDUTs9rrHUwY&start_date=";
                break;
            }
        }

    }
}
