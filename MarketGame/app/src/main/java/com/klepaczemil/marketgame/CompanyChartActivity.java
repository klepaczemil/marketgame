package com.klepaczemil.marketgame;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewDebug;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.mikephil.charting.charts.LineChart;

import java.util.Date;

import static android.R.id.edit;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;
import static com.klepaczemil.marketgame.R.id.showCompaniesButton;
import static com.klepaczemil.marketgame.R.id.spinner;
import static com.klepaczemil.marketgame.StartGameActivity.wallet;


public class CompanyChartActivity extends AppCompatActivity {


    String periodString = null;
    String apiCallLink;
    String companyId;
    boolean isFiiled = false;

    void showToast(CharSequence msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private void showNoMoneyAlert(EditText editText, TextView textView) {
        float money = StartGameActivity.wallet.getMoney();

        String textNumber = editText.getText().toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Lack of Founding!");
        builder.setMessage("You are not able to buy " + textNumber
                + " shares!\n" + "Your money: " + String.valueOf(money) + " USD.")
                .setCancelable(false)
                .setIcon(R.drawable.alert_ic)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showOkPurchaseAlert(EditText editText, TextView textView) {
        String textNumber = editText.getText().toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Successful Shares acquistion!");
        builder.setMessage("You acquired new shares: \n" + "amount: " + textNumber)
                .setCancelable(false)
                .setIcon(R.drawable.ok_ic)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_chart);
        //final TextView datasetTextView = (TextView) findViewById(R.id.datasetTextView);

        /*apiCallLink is get from previous activity where user choose Company*/
        Intent intent = getIntent();
        apiCallLink = intent.getExtras().getString("apiCallLink");
        /*companyId is get from previous activity e.x MSFT, GOOGL.*/
        companyId = intent.getExtras().getString("companyId");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Set chart time:");
        toolbar.setNavigationIcon(R.drawable.toolbarchart_ic);
        setSupportActionBar(toolbar);


        final LineChart chart = (LineChart) findViewById(R.id.chart);
        final ToggleButton toggleButtonFill = (ToggleButton) findViewById(R.id.toggleButtonFill);
        toggleButtonFill.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isFiiled = true;
                } else {
                    isFiiled = false;
                }
            }
        });


        final TextView previousCloseTextView = (TextView) findViewById(R.id.previousCloseTextView);

        final EditText getAmountEditText = (EditText) findViewById(R.id.getAmountEditText);
        Button buyButton = (Button) findViewById(R.id.buyButton);
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*if empty edit text*/
                String sNumberOfSharesToBuy = getAmountEditText.getText().toString();
                if (sNumberOfSharesToBuy.matches("")) {
                    Toast.makeText(getApplicationContext(), "You did not enter a number!", Toast.LENGTH_SHORT).show();
                    return;
                }


                int numberOfsharesToBuy = Integer.parseInt(getAmountEditText.getText().toString());
                float sharePrice = Float.parseFloat(previousCloseTextView.getText().toString());

                MoneyValidator mv = new MoneyValidator();

                if (mv.checkIfEnoughMoney(numberOfsharesToBuy, sharePrice, StartGameActivity.wallet.getMoney())) {
                    /*update wallet locally*/
                    mv.changeWalletAfterTransation(numberOfsharesToBuy, sharePrice, companyId, 1);
                    /*update wallet in DB*/
                    String sWalletID = String.valueOf(StartGameActivity.wallet.getWlletId());
                    String sNewAmount = String.valueOf(StartGameActivity.wallet.getNumberOfSharesById(companyId));
                    new UpdateWalletDB().execute(sWalletID, companyId, sNewAmount);

                    showOkPurchaseAlert(getAmountEditText, previousCloseTextView);
                } else {
                    showNoMoneyAlert(getAmountEditText, previousCloseTextView); //not enaugh money
                }


            }
        });


        Spinner spinner;
        final String[] periodsOfTime = {"5 days", "3 months", "6 months", "1 year", "5 years"};
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, periodsOfTime);
        spinner.setAdapter(spinnerAdapter);

        /*apply OnItemSelectListener for  spinner*/
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        String idString = Long.toString(id);


                        switch (position) {
                            case 0: {
                                DatesPeriod datesPeriod = new DatesPeriod(-5);
                                periodString = periodsOfTime[0];
                                new DatasetDownload(CompanyChartActivity.this, getApplicationContext(), chart, isFiiled, previousCloseTextView)
                                        .execute(datesPeriod.getStartDate(), datesPeriod.getEndDate(), apiCallLink);
                                break;
                            }
                            case 1: {
                                DatesPeriod datesPeriod = new DatesPeriod(-90);
                                periodString = periodsOfTime[1];
                                new DatasetDownload(CompanyChartActivity.this, getApplicationContext(), chart, isFiiled, previousCloseTextView)
                                        .execute(datesPeriod.getStartDate(), datesPeriod.getEndDate(), apiCallLink);
                                break;
                            }

                            case 2: {
                                DatesPeriod datesPeriod = new DatesPeriod(-180);
                                periodString = periodsOfTime[2];
                                new DatasetDownload(CompanyChartActivity.this, getApplicationContext(), chart, isFiiled, previousCloseTextView)
                                        .execute(datesPeriod.getStartDate(), datesPeriod.getEndDate(), apiCallLink);
                                break;
                            }
                            case 3: {
                                DatesPeriod datesPeriod = new DatesPeriod(-365);
                                periodString = periodsOfTime[3];
                                new DatasetDownload(CompanyChartActivity.this, getApplicationContext(), chart, isFiiled, previousCloseTextView)
                                        .execute(datesPeriod.getStartDate(), datesPeriod.getEndDate(), apiCallLink);
                                break;
                            }
                            case 4: {
                                DatesPeriod datesPeriod = new DatesPeriod(-1825);
                                periodString = periodsOfTime[4];
                                new DatasetDownload(CompanyChartActivity.this, getApplicationContext(), chart, isFiiled, previousCloseTextView)
                                        .execute(datesPeriod.getStartDate(), datesPeriod.getEndDate(), apiCallLink);
                                break;
                            }
                        }
                        showToast("Chart period: " + periodString + " selected");
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        showToast("Chart period: unselected");
                    }
                });
        ;


    }

}
