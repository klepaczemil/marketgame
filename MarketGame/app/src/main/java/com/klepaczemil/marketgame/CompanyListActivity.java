package com.klepaczemil.marketgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class CompanyListActivity extends AppCompatActivity {

    private ArrayList<Company> makeCompanies(){
        ArrayList<Company> companies = new ArrayList<Company>();

        companies.add(new Company("MSFT","Microsoft (MSFT)", getResources().getString(R.string.msft_description) , "msft48_logo"));
        companies.add(new Company("GOOGL","Google Inc. (Google)", getResources().getString(R.string.google_description), "google48_logo"));
        companies.add(new Company("AAPL","Apple Inc. (AAPL)", getResources().getString(R.string.apple_description),"apple48_logo"));
        companies.add(new Company("FB", "Facebook Inc.(Facebook)", getResources().getString(R.string.facebook_description),"facebook48_logo"));
        companies.add(new Company("BHLB","Berkshire Hills", getResources().getString(R.string.berkshire_description), "berkshirebank_logo"));
        companies.add(new Company("AMZN", "Amazon.com (AMZN)", getResources().getString(R.string.amazon_description), "amazon48_logo"));
        companies.add(new Company("GNE", "General Electric (GE)", getResources().getString(R.string.ge_description), "ge48_logo"));
        companies.add(new Company("JNJ", "Johnson & Johnson", getResources().getString(R.string.johnsonandjohnson_description), "johnson48_logo"));
        return companies;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_list);

        final ArrayList<Company> companies = makeCompanies();
        ListView companyListView = (ListView)findViewById(R.id.companyListView);
        companyListView.setAdapter(new CompanyAdapter(this, companies));


        companyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent companychartIntent = new Intent(getApplicationContext(), CompanyChartActivity.class);

                final String apiCallLink = companies.get(position).getApiCallLink();
                companychartIntent.putExtra("apiCallLink", apiCallLink);

                final String companyId = companies.get(position).getId();
                companychartIntent.putExtra("companyId", companyId);

                startActivity(companychartIntent);

            }
        });

    }
}
