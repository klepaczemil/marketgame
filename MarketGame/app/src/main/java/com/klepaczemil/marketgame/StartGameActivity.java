package com.klepaczemil.marketgame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import static com.klepaczemil.marketgame.R.id.companyListView;
import static com.klepaczemil.marketgame.R.id.showCompaniesButton;

public class StartGameActivity extends AppCompatActivity {

    public static User user;
    public static Wallet wallet;

    private ArrayList<Details> makeDetails(Wallet wallet) {
        ArrayList<Details> details = new ArrayList<Details>();
        details.add(new Details("MSFT", wallet.getMSFT(), getResources().getString(R.string.link_MSFT)));
        details.add(new Details("GOOGL", wallet.getGOOGL(), getResources().getString(R.string.link_GOOGL)));
        details.add(new Details("AAPL", wallet.getAAPL(), getResources().getString(R.string.link_AAPL)));
        details.add(new Details("FB", wallet.getFB(), getResources().getString(R.string.link_FB)));
        details.add(new Details("BHLB", wallet.getBHLB(), getResources().getString(R.string.link_BHLB)));
        details.add(new Details("AMZN", wallet.getAMZN(), getResources().getString(R.string.link_AMZN)));
        details.add(new Details("GNE", wallet.getGNE(), getResources().getString(R.string.link_GNE)));
        details.add(new Details("JNJ", wallet.getJNJ(), getResources().getString(R.string.link_JNJ)));
        return details;
    }

    /*function is needed for update UI whem wallet content change*/
    private void moneyUpdate() {
        TextView moneyTextViev = (TextView) findViewById(R.id.moneyTextView);
        String formattedMoneyString = String.format("%.2f", wallet.getMoney());
        System.out.println(formattedMoneyString);
        moneyTextViev.setText(formattedMoneyString + " USD");
    }

    private void detailsUpdate() {
        final ArrayList<Details> details = makeDetails(wallet);
        ListView detailsListView = (ListView) findViewById(R.id.detailsListView);
        detailsListView.setAdapter(new DetailsAdapter(this, details));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_game);

        /*unparceling data from previous activity - directly from DB on remote server*/
        Bundle userData = getIntent().getExtras();
        user = (User) userData.getParcelable("user");
        wallet = (Wallet) userData.getParcelable("wallet");

        Button rankingButton = (Button) findViewById(R.id.rankingButton);
        rankingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent rankingIntent = new Intent(getApplicationContext(), RankingActivity.class);
                startActivity(rankingIntent);
            }
        });

        final ArrayList<Details> details = makeDetails(wallet);
        ListView detailsListView = (ListView) findViewById(R.id.detailsListView);
        detailsListView.setAdapter(new DetailsAdapter(this, details));

        detailsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent sellIntent = new Intent(getApplicationContext(), SellActivity.class);

                final String companyId = details.get(position).getCode();
                sellIntent.putExtra("companyId", companyId);

                final String linkApi = details.get(position).getApiLink();
                sellIntent.putExtra("linkApi", linkApi);
                startActivity(sellIntent);

            }
        });

        moneyUpdate();

        Button showCompaniesButton = (Button) findViewById(R.id.showCompaniesButton);
        showCompaniesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent companyListIntent = new Intent(getApplicationContext(), CompanyListActivity.class);
                startActivity(companyListIntent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        moneyUpdate();
        detailsUpdate();
    }

}
