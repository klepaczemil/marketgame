package com.klepaczemil.marketgame;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SellActivity extends AppCompatActivity {

    String companyId;
    String linkApi;
    int numberOfShares;
    public static float currentSharePrice;

    private void updateNumberOfShares() {
        numberOfShares = StartGameActivity.wallet.getNumberOfSharesById(companyId);
        TextView numberOfSharesTextView = (TextView) findViewById(R.id.amountTextView);
        numberOfSharesTextView.setText(String.valueOf(numberOfShares));

    }

    private void updateCurrentPrice() {
        TextView currentPriceTextView = (TextView) findViewById(R.id.priceTextView);
        new DownloadCurrentPrice(SellActivity.this, currentPriceTextView).execute(linkApi);

    }

    private void showOkSellAllert(EditText editText, int numberOfShares) {
        String textNumber = editText.getText().toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Successful Shares sale!");
        builder.setMessage("You sold shares: \n" + "amount: " + textNumber)
                .setCancelable(false)
                .setIcon(R.drawable.ok_ic)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void showLackSharesAlert(EditText editText, int numberOfShares) {
        float money = StartGameActivity.wallet.getMoney();

        String textNumber = editText.getText().toString();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Lack of Shares!");
        builder.setMessage("You are not able to sell " + textNumber
                + " shares!\n" + "Your shares: " + numberOfShares + " .")
                .setCancelable(false)
                .setIcon(R.drawable.alert_ic)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_acticity);

        Intent intent = getIntent();
        companyId = intent.getExtras().getString("companyId");
        linkApi = intent.getExtras().getString("linkApi");

        TextView idTextView = (TextView) findViewById(R.id.idTextView);
        idTextView.setText(companyId);

        final EditText numberToSellET = (EditText) findViewById(R.id.numberToSellEditText);


        Button sellButton = (Button) findViewById(R.id.sellButton);
        sellButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sNumberToSell = numberToSellET.getText().toString();

                /*if empty edit text*/
                if (sNumberToSell.matches("")) {
                    Toast.makeText(getApplicationContext(), "You did not enter a number!", Toast.LENGTH_SHORT).show();
                    return;
                }

                final int numberToSell = Integer.parseInt(numberToSellET.getText().toString());
                MoneyValidator mv = new MoneyValidator();
                if (mv.checkIfEnoughSheresToSell(companyId, numberToSell)) {
                    /*update locally*/
                    showOkSellAllert(numberToSellET, numberOfShares);
                    mv.changeWalletAfterTransation(numberToSell, currentSharePrice, companyId, 0 );

                    /*update in database*/
                    String sWalletID = String.valueOf(StartGameActivity.wallet.getWlletId());
                    String sNewAmount = String.valueOf(StartGameActivity.wallet.getNumberOfSharesById(companyId));
                    new UpdateWalletDB().execute(sWalletID, companyId, sNewAmount);
                    updateNumberOfShares();
                } else {
                    showLackSharesAlert(numberToSellET, numberOfShares);
                }
            }
        });

        updateNumberOfShares();
        updateCurrentPrice();


    }

    @Override
    protected void onResume() {
        super.onResume();
        updateNumberOfShares();
        updateCurrentPrice();
    }

}
