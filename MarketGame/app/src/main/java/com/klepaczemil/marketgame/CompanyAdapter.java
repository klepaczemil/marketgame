package com.klepaczemil.marketgame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

public class CompanyAdapter extends BaseAdapter {
    Context context;
    ArrayList<Company> companies;
    LayoutInflater inflater;

    public CompanyAdapter(Context context, ArrayList<Company> companies) {
        this.context = context;
        this.companies = companies;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return companies.size();
    }

    public Object getItem(int position) {
        return companies.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.comapny_view_item, null);
        TextView largerTextView = (TextView) view.findViewById(R.id.largerTextView);
        TextView smallerTextView = (TextView) view.findViewById(R.id.smallerTextView);
        ImageView logoimageView = (ImageView) view.findViewById(R.id.logoimageView);
        Company currentCompany = companies.get(position);
        largerTextView.setText(currentCompany.name);
        smallerTextView.setText(currentCompany.description);

        int drawableResourceLogoId = context.getResources().getIdentifier(currentCompany.logoNameDrawable, "drawable", context.getPackageName());
        logoimageView.setImageResource(drawableResourceLogoId);
        return view;
    }
    

}
