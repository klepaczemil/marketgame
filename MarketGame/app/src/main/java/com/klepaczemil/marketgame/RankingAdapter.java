package com.klepaczemil.marketgame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static android.media.CamcorderProfile.get;
import static com.klepaczemil.marketgame.R.id.largerTextView;
import static com.klepaczemil.marketgame.R.id.logoimageView;
import static com.klepaczemil.marketgame.R.id.moneyTextView;
import static com.klepaczemil.marketgame.R.id.rankingEmailTextView;
import static com.klepaczemil.marketgame.R.id.rankingPositionTextView;
import static com.klepaczemil.marketgame.R.id.smallerTextView;

/**
 * Created by emil_k on 2016-11-14.
 */

public class RankingAdapter extends BaseAdapter {

    Context context;
    ArrayList<RankingPosition> rankingList;
    LayoutInflater inflater;

    public RankingAdapter(Context context, ArrayList<RankingPosition> rankingList) {
        this.context = context;
        this.rankingList = rankingList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return rankingList.size();
    }

    public Object getItem(int position) {
        return rankingList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflater.inflate(R.layout.ranking_row, null);
        TextView rankingPosition = (TextView) view.findViewById(rankingPositionTextView);
        TextView emailTextView = (TextView) view.findViewById(rankingEmailTextView);
        TextView moneyTextView = (TextView) view.findViewById(R.id.rankingMoneyTextView);

        RankingPosition currentRankingPos = rankingList.get(position);
        rankingPosition.setText((position + 1) + ".");
        emailTextView.setText(currentRankingPos.email);
        moneyTextView.setText(String.valueOf(currentRankingPos.money + "$"));
        return view;
    }
}
