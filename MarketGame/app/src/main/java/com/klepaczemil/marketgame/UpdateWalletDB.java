package com.klepaczemil.marketgame;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.klepaczemil.marketgame.StartGameActivity.wallet;

public class UpdateWalletDB extends AsyncTask<String, Void, Void> {

    private Activity activity;
    private Context context;
    private boolean isUpdateOK = false;

    private boolean updateShares(int walletId, String companyId, int newAmonut) throws IOException {
        String linkUpdateWallet = "https://marketgamecom.000webhostapp.com/updateWalletScript.php?"
                + "walletId=" + walletId + "&companyId=" + companyId +
                "&newAmount=" + newAmonut;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodUpdateW = new HttpGet(linkUpdateWallet);
        HttpResponse response = httpClient.execute(getMethodUpdateW);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";

        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();

        if (sb.toString().equals("1")) {
            return true;
        }
        return false;
    }

    private boolean updateMoney() throws IOException{
        int walletId = StartGameActivity.wallet.getWlletId();
        float money = StartGameActivity.wallet.getMoney();
        String sMoney = String.valueOf(money);
        String linkUpdateMoney = "https://marketgamecom.000webhostapp.com/updateMoneyScript.php?" + "walletId="+ walletId + "&newMoney=" + sMoney;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodUpdateM = new HttpGet(linkUpdateMoney);
        HttpResponse response = httpClient.execute(getMethodUpdateM);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";

        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();

        if (sb.toString().equals("1")) {
            return true;
        }
        return false;
    }

    @Override
    protected Void doInBackground(String... arg0) {
        String walletId = arg0[0];
        String companyId = arg0[1];
        String newAmount = arg0[2];
        try{
            isUpdateOK = updateShares(Integer.parseInt(walletId), companyId, Integer.parseInt(newAmount));
            isUpdateOK = updateMoney();
        }catch (IOException e){
            e.printStackTrace();
        }


        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        if(isUpdateOK){
            System.out.println("shares update");
        }

    }
}
