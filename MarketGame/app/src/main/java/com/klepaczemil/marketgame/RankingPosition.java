package com.klepaczemil.marketgame;

public class RankingPosition {
    String email;
    float money;

    public RankingPosition(String email, float money) {
        this.email = email;
        this.money = money;
    }

    public RankingPosition() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "RankingPosition{" +
                "email='" + email + '\'' +
                ", money=" + money +
                '}';
    }
}
