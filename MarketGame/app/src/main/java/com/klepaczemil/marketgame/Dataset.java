package com.klepaczemil.marketgame;


import java.util.ArrayList;

public class Dataset {
    private String dataset_code;
    private String name;
    private String description;
    private String start_date;
    private String end_date;
    private ArrayList<Data> dataList;

    public Dataset(String dataset_code, String name, String description, String start_date, String end_date, ArrayList<Data> dataList) {
        this.dataset_code = dataset_code;
        this.name = name;
        this.description = description;
        this.start_date = start_date;
        this.end_date = end_date;
        this.dataList = dataList;
    }

    public Dataset(){

    }

    public String getDataset_code() {
        return dataset_code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public ArrayList<Data> getDataList() {
        return dataList;
    }



    public void setDataset_code(String dataset_code) {
        this.dataset_code = dataset_code;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public void setDataList(ArrayList<Data> dataList) {
        this.dataList = dataList;
    }

    @Override
    public String toString() {
        return "Dataset{" +
                "dataset_code='" + dataset_code + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", dataList=" + dataList +
                '}';
    }
}
