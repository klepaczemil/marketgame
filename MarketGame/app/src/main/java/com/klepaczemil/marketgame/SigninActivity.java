package com.klepaczemil.marketgame;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import static com.klepaczemil.marketgame.R.id.email;


public class SigninActivity extends AsyncTask<String, Void, String> {
    private TextView statusField;
    private ProgressDialog progressDialog;
    private Activity activity;
    private Context context;
    private boolean loginOK = false;
    private User user = new User();
    private Wallet wallet= new Wallet();


    public SigninActivity(Activity activity, Context context, TextView statusField) {
        this.statusField = statusField;
        /*Activity that is previous acitity wchich passes parameters (including itself) to execute method*/
        /*for this async SigninActivity important to invoke ProgressDialog*/
        this.activity = activity;
        this.context = context;
    }


    private boolean checkIfLoginExists(String email) throws IOException {
        /*Script checking if Login already exists in DB */
        String linkCheckEmail = "https://marketgamecom.000webhostapp.com/checkIfLoginExistsScript.php" + "?" + "email=" + email;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodCheckEmail = new HttpGet(linkCheckEmail);
        HttpResponse response = httpClient.execute(getMethodCheckEmail);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";

        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();

        if (sb.toString().equals("1")) {
            return true;
        }
        return false;
    }

    private boolean checkIsPasswordCorrect(String email, String password) throws IOException {
        /*check is password correct*/
        String linkCheckPass = "https://marketgamecom.000webhostapp.com/checkIsPasswordCorrectScript.php" + "?" + "email=" + email + "&password=" + password;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodCheckPass = new HttpGet(linkCheckPass);
        HttpResponse response = httpClient.execute(getMethodCheckPass);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";

        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();

        if (sb.toString().equals("1")) {
            return true;
        }
        return false;
    }

    private void addNewUser(String email, String password) throws IOException {
        /*Script addding new user php */
        String linkAdd = "https://marketgamecom.000webhostapp.com/adduserTest.php" + "?" + "email=" + email + "&password=" + password;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodAdd = new HttpGet(linkAdd);
        httpClient.execute(getMethodAdd);
    }

    private int getUserIdByEmail(String email)throws IOException{
        String linkCheckPass = "https://marketgamecom.000webhostapp.com/returnUserIdByEmailScript.php" + "?" + "email=" + email;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodGetUser = new HttpGet(linkCheckPass);
        HttpResponse response = httpClient.execute(getMethodGetUser);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";

        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();

        int userId = Integer.parseInt(sb.toString());
        return userId;
    }

    private int getWalletIdByEmail(String email)throws IOException{
        String linkCheckWalletId = "https://marketgamecom.000webhostapp.com/getWalletIdByEmailScript.php" + "?" + "email=" + email;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodGetWalletId = new HttpGet(linkCheckWalletId);
        HttpResponse response = httpClient.execute(getMethodGetWalletId);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuffer sb = new StringBuffer("");
        String line = "";

        while ((line = in.readLine()) != null) {
            sb.append(line);
        }
        in.close();

        int WalletId = Integer.parseInt(sb.toString());
        return WalletId;
    }

    private Wallet getWalletByWalletId(int walletId)throws IOException{
        String linkCheckWalletId = "https://marketgamecom.000webhostapp.com/getWalletContentByWalletIdScript.php" + "?" + "WalletId=" + walletId;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethodGetWalletId = new HttpGet(linkCheckWalletId);
        HttpResponse response = httpClient.execute(getMethodGetWalletId);

        BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line = "";

        ArrayList<String> walletAtributes = new ArrayList<>();
        while ((line = in.readLine()) != null) {
            walletAtributes.add(line);
        }
        in.close();

        float money = Float.parseFloat(walletAtributes.get(0));
        int MSFT = Integer.parseInt(walletAtributes.get(1));
        int GOOGL = Integer.parseInt(walletAtributes.get(2));
        int AAPL = Integer.parseInt(walletAtributes.get(3));
        int FB = Integer.parseInt(walletAtributes.get(4));
        int BHLB = Integer.parseInt(walletAtributes.get(5));
        int AMZN = Integer.parseInt(walletAtributes.get(6));
        int GNE = Integer.parseInt(walletAtributes.get(7));
        int JNJ = Integer.parseInt(walletAtributes.get(8));

        Wallet wallet = new Wallet(walletId, money, MSFT, GOOGL, AAPL, FB, BHLB, AMZN, GNE, JNJ);
        return wallet;

    }

    protected void onPreExecute() {
        progressDialog = new ProgressDialog(activity);
        progressDialog.setTitle("Data processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(true);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SigninActivity.this.cancel(true);
                progressDialog.dismiss();

            }
        });
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        try {
            String email = (String) arg0[0];
            String password = (String) arg0[1];
            String result = "";

            user.setEmail(email);
            user.setPassword(password);

            /*If sb = 1 e-mail already exists if sb = 0 not exists */
            if (checkIfLoginExists(email)) {
                if (checkIsPasswordCorrect(email, password)) {
                    result += "Password correct. Login sucess";
                    user.setUserId(getUserIdByEmail(email));
                    user.setWalletId(getWalletIdByEmail(email));
                    wallet = getWalletByWalletId(getWalletIdByEmail(email));
                    loginOK = true;
                } else {
                    result += "Given Password IS NOT correct. Try again. ";
                }
                return result;

            } else {
                addNewUser(email, password);
                result = "Congratulations. You have sucessfully registered!";
                user.setUserId(getUserIdByEmail(email));
                user.setWalletId(getWalletIdByEmail(email));
                wallet = getWalletByWalletId(getWalletIdByEmail(email));
                loginOK = true;
                return result;

            }

        } catch (IOException e) {
            e.printStackTrace();
            return e.toString();
        }

    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();

        /*Toast on the bottom of device*/
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, result, duration);
        toast.show();

        if (loginOK) {
            Intent startGameActivityIntent = new Intent(context, StartGameActivity.class);
            /*paassing user object to next activity StartGameActivity*/
            startGameActivityIntent.putExtra("user", user);
            startGameActivityIntent.putExtra("wallet", wallet);
            activity.startActivity(startGameActivityIntent);
        }

    }
}
