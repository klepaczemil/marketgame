package com.klepaczemil.marketgame;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static android.content.ContentValues.TAG;

public class HttpHandler {
    public String makeServiceCall(String urlAPI) {
        String response = null;

        HttpClient httpClient = new DefaultHttpClient();
        HttpGet getMethod = new HttpGet(urlAPI);
        try {
            HttpResponse httpResponse = httpClient.execute(getMethod);

            BufferedReader in = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = in.readLine()) != null) {
                sb.append(line).append('\n');
            }
            response = sb.toString();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(TAG, "Exeption" + e.getMessage());
        }

        return response;

    }

}
